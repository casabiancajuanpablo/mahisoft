import os

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

def check_palindrome(num):
    return str(num) == str(num)[::-1]


def answer1(num):
    found = False
    newNum = num
    while not found:
        if not check_palindrome(newNum):
            newNum += 1
        else:
            found = True
            return newNum


def check_iteration(arr):

    lastNum = tiempos[-1]
    lastNumIteration = lastNum
    found = False

    while not found:

        checked = True

        for i in range(0, (len(tiempos)-1)):
            if lastNumIteration % tiempos[i] != 0:
                checked = False
                break

        if checked:
            found = True
            return lastNumIteration
        else:
            lastNumIteration += lastNum


def es_omirp(num):
    revNum = int(str(num)[::-1])
    for i in range(3, revNum):
        if revNum % i == 0:
            return False
    return True


if __name__ == '__main__':

    loop = True
    error = None

    while loop:

        clear()
        print("Problems:")
        print("Mahisoft Test (Press 'q' to quit)")

        if error:
            print(error)

        print("a) Problem 1")
        print("b) Problem 2")
        print("c) Problem 3")
        action = raw_input('\nPlease choose the problem: ').lower().strip()

        if action == 'a':

            clear()
            print('Solution #1:')
            numbers = [12, 1234, 986]
            for number in numbers:
                msg = 'The solution for {} is: {}'.format(number, answer1(number))
                print(msg)
            raw_input('\nPress [Enter] to go back: ')

        elif action == 'b':

            clear()
            print('Solution #2:')
            tiempos = [1,3,4]
            res = check_iteration(tiempos)
            msg = 'The solution for {} is: {}'.format(tiempos, res)
            print(msg)
            raw_input('\nPress [Enter] to go back: ')

        elif action == 'c':

            clear()
            print('Solution #3:')
            num = 92
            if es_omirp(num):
                print('Wow! {} is an omirp number: {}!').format(num, str(num)[::-1])
            else:
                print('Oh Darn! {} is not an omirp number.').format(num)
            raw_input('\nPress [Enter] to go back: ')

        elif action == 'q':
            clear()
            loop = False
            break
        else:
            error = 'Please choose a valid option!'
